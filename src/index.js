import helloWorld from './components/hello-world';
import './css/style.css';

const element = document.createElement('div');

element.innerHTML = helloWorld();
element.className = 'hello-world';

document.body.appendChild(element);

if (module.hot) {
  module.hot.accept('./components/hello-world.js', function() {
    console.log('Accepting the updated hello module!');
    element.innerHTML = helloWorld();
  });
}
