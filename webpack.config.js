const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');

module.exports = env => {
  const isDevelopment = env && env.dev;
  const config = {
    mode: isDevelopment ? 'development' : 'production',
    entry: './index',
    context: path.resolve(__dirname, 'src'),
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },
    devtool: isDevelopment ? 'source-map' : false,
    devServer: {
      contentBase: path.join(__dirname, "dist"),
      port: 9000,
      hot: isDevelopment
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',

          options: {
            presets: [['env', { modules: isDevelopment ? false : 'commonjs' }]]
          }
        },
        {
          test: /\.css$/,

          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  sourceMap: isDevelopment
                }
              }
            ],
            fallback: 'style-loader'
          })
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]'
              }
            }
          ]
        }
      ]
    },

    plugins: [
      new ExtractTextPlugin({
        disable: isDevelopment,
        filename: 'css/style.css',
        allChunks: true
      })
    ]
  };

  if (isDevelopment) {
    config.plugins.push(
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin()
    );
  }

  return config;
};
